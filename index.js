///////////JavaScript Functions//////////////
function getDetails(firstName,lastName,age,currentJob,reason,goals){

	return `Hello, my name is ${firstName} ${lastName}. I am ${age} years old. I am currently working on a company as a ${currentJob}. I wanted to pursue this new career to ${reason}. I am planning to ${goals}`;
}

/////////////////////////////////////
/////////////////If Else Statement////////////////////
let isNotValid = true;
while(isNotValid){
	let subscription= prompt("Please enter your desired subscription:\n-Basic\n-Premium");
	if(subscription.toLowerCase()=="basic"){
		alert("you have chosen the basic package, you will be able to use our service only on a single device.");
		isNotValid = false;
	}else if(subscription.toLowerCase()=="premium"){
		alert("you have chosen the premium package, enjoy the full potential of our service on up to 3 devices.");
		isNotValid = false;
	}else{
		alert("please enter a valid keyword");
	}
}
/////////////////////////////////////
////////////////Ternary Operator////////////////////
function useTernary(sub){
		sub.toLowerCase()=="basic"?alert("you have chosen the basic package, you will be able to use our service only on a single device."):
		sub.toLowerCase()=="premium"?alert("you have chosen the premium package, enjoy the full potential of our service on up to 3 devices."):
		alert("please enter a valid keyword");
}
/////////////////////////////////////
////////////////Switch Case Statement?/////////////////////
function submitDetails(){
	let	firstName = document.getElementById("firstName").value;
	let	lastName = document.getElementById("lastName").value;
	let	age = document.getElementById("age").value;
	let	currentJob = document.getElementById("currentWork").value;
	let	reason = document.getElementById("reason").value;
	let	goal = document.getElementById("goal").value;
	let doDisplay = false;
	if(firstName ==""||lastName ==""|| age ==""||currentJob ==""||reason ==""||goal ==""){
		document.getElementById("summary").innerHTML = "Please complete the form";
	}else{
		doDisplay = true;
	}
	switch(doDisplay){
		case true: document.getElementById("summary").innerHTML = getDetails(firstName,lastName,age,currentJob,reason,goal); break;
		case false: document.getElementById("summary").innerHTML = "Please complete the form";
	}
}
//////////////LOOPS///////////////////////
let str = "Mamamia!";
let arr = [];
for(let x = 0; x<str.length;x++){
	arr.push(str[x]);
}
console.log(str,arr);


///////////////ARRAY//////////////////////
let studentArr = ["viego","sion","kindred","ahri"];

console.log("this is the array",studentArr);
console.log("this array is "+studentArr.length+" element long.");
console.log("this is the last element on this array:",studentArr[studentArr.length-1]);
console.log("this is the array now because I added an element on the front:");
studentArr.unshift("garen");
console.log(studentArr);
console.log("now I added an element on the end:");
studentArr.push("viktor");
console.log(studentArr);
console.log("now I removed the first element:");
studentArr.shift();
console.log(studentArr);
console.log("now I removed the last element:");
studentArr.pop();
console.log(studentArr);
/////////////////////////////////////
////////////////OBJECTS/////////////////////

let person={
	firstName: "chester",
	lastName: "clavio",
	age: 24,
	address: "venice runeterra",
	references: [{name:'viego',email:'viego@rterra.com'},{name:'karma',email:'karma@rterra.com'},{name:'druttut',email:'druttut@rterra.com'}],
	friends: ['sion','warwick','ahri'],
	admin: true,
	print: function(){
		console.log(`Hello, my name is ${this.firstName} ${this.lastName}. I am ${this.age} years old. I live in ${this.address}.`)();
	}
};

/////////////////////////////////////
////////////////ARROW FUNCTION/////////////////////

let test = obj => `Hello ${obj.firstName} ${obj.lastName}, welcome to our service.
We would like to inform you that your items will be delivered on your address which is ${obj.address}.
Thank you for choosing our service, have a blessed day!`;

console.log(test(person));